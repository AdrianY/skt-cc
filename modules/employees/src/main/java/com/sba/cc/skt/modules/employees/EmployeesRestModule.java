package com.sba.cc.skt.modules.employees;

import com.sba.cc.skt.modules.employees.implementation.EmployeesRestImplementation;
import com.sba.cc.skt.modules.employees.infra.EmployeeRestInfra;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        EmployeesRestImplementation.class,
        EmployeeRestInfra.class
})
public class EmployeesRestModule {
}
