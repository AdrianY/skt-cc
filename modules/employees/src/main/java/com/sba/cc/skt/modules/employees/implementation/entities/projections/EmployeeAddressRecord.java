package com.sba.cc.skt.modules.employees.implementation.entities.projections;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "employeeAddressRecord", types = EmployeeAddressRecord.class)
public interface EmployeeAddressRecord {
    String getCity();

    String getStreet();

    String getZipcode();
}
