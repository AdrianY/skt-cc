package com.sba.cc.skt.modules.employees.implementation.entities.projections;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sba.cc.skt.modules.employees.implementation.entities.Employee;
import com.sba.cc.skt.modules.employees.implementation.entities.EmployeeAddress;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;
import java.time.LocalDate;

@Projection(name = "employeeRecord", types = Employee.class)
public interface EmployeeRecord {

    String getName();

    String getEmail();

    String getPhone();

    String getCompany();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MMM dd, yyyy")
    LocalDate getJoindate();

    EmployeeAddressRecord getAddress();

    Integer getAge();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#,##0.###")
    BigDecimal getSalary();
}
