package com.sba.cc.skt.modules.employees.implementation.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sba.cc.skt.commons.data.api.entities.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "employee")
public class Employee extends BaseEntity implements Serializable {

    @Column
    private String name;

    /**
     * No mutation feature (e.g. create or update) via API
     * so there is no need to introduce a service layer which handles
     * serialization. Thus, JsonFormat is embedded here for the sake
     * of bootstrapping
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    @Column(name = "join_date")
    private LocalDate joindate;

    /**
     * Not using a lower data type since CPUs are optimised
     * to handle 32 bit data nowadays
     */
    @Column
    private Integer age;

    @Column
    private String company;

    @Column
    private String email;

    @Column
    private String phone;

    @Column
    private BigDecimal salary;

    @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
    private EmployeeAddress address;

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public LocalDate getJoindate() {
        return joindate;
    }

    public Employee setJoindate(LocalDate joindate) {
        this.joindate = joindate;
        return this;
    }

    public Integer getAge() {
        return age;
    }

    public Employee setAge(Integer age) {
        this.age = age;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public Employee setCompany(String company) {
        this.company = company;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Employee setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Employee setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public Employee setSalary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    public EmployeeAddress getAddress() {
        return address;
    }

    public Employee setAddress(EmployeeAddress address) {
        this.address = address;
        this.address.setEmployee(this);
        return this;
    }
}
