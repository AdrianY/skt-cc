package com.sba.cc.skt.modules.employees.implementation.entities.projections;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sba.cc.skt.modules.employees.implementation.entities.Employee;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;

@Projection(name = "employeeCard", types = Employee.class)
public interface EmployeeCard {
    String getName();

    String getEmail();

    String getPhone();

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "#,##0.###")
    BigDecimal getSalary();

    Integer getAge();

    @Value("#{target.address != null && (target.address.street != null || target.address.city != null ) ? (" +
            "(target.address.street ?: '') +" +
            "(target.address.street != null && target.address.city != null ? ', ' : '') +" +
            "(target.address.city ?: '') ) : " +
            "null}")
    String getAddress();
}
