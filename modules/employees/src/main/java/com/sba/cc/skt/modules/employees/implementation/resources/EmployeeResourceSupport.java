package com.sba.cc.skt.modules.employees.implementation.resources;

import com.sba.cc.skt.modules.employees.implementation.entities.Employee;
import com.sba.cc.skt.modules.employees.implementation.entities.projections.EmployeeCard;
import com.sba.cc.skt.modules.employees.implementation.entities.projections.EmployeeRecord;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.BaseUri;
import org.springframework.data.rest.webmvc.ProfileController;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class EmployeeResourceSupport {

    private final ProjectionFactory projectionFactory;
    private final RepositoryRestConfiguration configuration;

    public EmployeeResourceSupport(ProjectionFactory projectionFactory, RepositoryRestConfiguration configuration) {
        this.projectionFactory = projectionFactory;
        this.configuration = configuration;
    }

    private static String getProfilePath(RepositoryRestConfiguration configuration) {
        BaseUri baseUri = new BaseUri(configuration.getBaseUri());
        String rootPath = baseUri.getUriComponentsBuilder().path(ProfileController.PROFILE_ROOT_MAPPING)
                .build().toString();
        return rootPath + "/employees";
    }

    Resource<EmployeeRecord> wrapResource(Employee employee){
        return new Resource<>(
                projectionFactory.createProjection(EmployeeRecord.class, employee),
                linkTo(methodOn(EmployeeResource.class)
                        .findOne(employee.getId())).withSelfRel(),
                linkTo(methodOn(EmployeeResource.class).findAll())
                        .withRel("employees"),
                linkTo(methodOn(EmployeeResource.class).findAllByOrderBySalaryAsc())
                        .withRel("employees-sorted-by-salary"),
                linkTo(methodOn(EmployeeResource.class).findAllByAgeYoungerThan(employee.getAge()))
                        .withRel("employees-younger-than")
        );
    }

    Resource<EmployeeCard> wrapResourceCollectionItem(Employee employee){
        return new Resource<>(
                projectionFactory.createProjection(EmployeeCard.class, employee),
                linkTo(methodOn(EmployeeResource.class)
                        .findOne(employee.getId())).withSelfRel(),
                linkTo(methodOn(EmployeeResource.class).findAllByAgeYoungerThan(employee.getAge()))
                        .withRel("employees-younger-than")
        );
    }

    Resources<Resource<EmployeeCard>> wrapResources(
            List<Resource<EmployeeCard>> employeeResources,
            Link... links
    ){
        List<Link> halLinks = new ArrayList<>(Arrays.asList(links));
        halLinks.add(new Link(getProfilePath(configuration), "profile"));
        return new Resources<>(employeeResources, halLinks);
    }

}
