package com.sba.cc.skt.modules.employees.implementation.entities;

import com.sba.cc.skt.commons.data.api.entities.BaseDependentEntity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "employee_address")
public class EmployeeAddress extends BaseDependentEntity {

    @Column
    private String city;

    @Column
    private String street;

    @Column
    private String zipcode;

    @MapsId
    @OneToOne
    private Employee employee;

    public String getCity() {
        return city;
    }

    public EmployeeAddress setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public EmployeeAddress setStreet(String street) {
        this.street = street;
        return this;
    }

    public String getZipcode() {
        return zipcode;
    }

    public EmployeeAddress setZipcode(String zipcode) {
        this.zipcode = zipcode;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }


}
