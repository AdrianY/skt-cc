package com.sba.cc.skt.modules.employees.infra;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = "com.sba.cc.skt.modules.employees.implementation.entities")
@EnableJpaRepositories(basePackages = "com.sba.cc.skt.modules.employees.implementation.repository")
public class EmployeeRestInfra {
}
