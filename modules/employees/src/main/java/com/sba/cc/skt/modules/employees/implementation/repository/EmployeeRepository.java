package com.sba.cc.skt.modules.employees.implementation.repository;

import com.sba.cc.skt.modules.employees.implementation.entities.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(
        path = "employees",
        collectionResourceRel = "employees"
)
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    @RestResource(exported = false)
    List<Employee> findAllByOrderBySalaryAsc();

    @RestResource(exported = false)
    List<Employee> findAllByAgeLessThan(Integer age);

    @Override
    @RestResource(exported = false)
    <S extends Employee> S save(S entity);

    @Override
    @RestResource(exported = false)
    void delete(Long aLong);

    @Override
    @RestResource(exported = false)
    void delete(Employee entity);
}
