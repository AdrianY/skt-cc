package com.sba.cc.skt.modules.employees.implementation.resources;

import com.sba.cc.skt.modules.employees.implementation.entities.Employee;
import com.sba.cc.skt.modules.employees.implementation.entities.projections.EmployeeCard;
import com.sba.cc.skt.modules.employees.implementation.entities.projections.EmployeeRecord;
import com.sba.cc.skt.modules.employees.implementation.repository.EmployeeRepository;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RepositoryRestController
public class EmployeeResource {

    private final EmployeeResourceSupport resourceSupport;
    private final EmployeeRepository employeeRepository;

    public EmployeeResource(EmployeeResourceSupport resourceSupport, EmployeeRepository employeeRepository) {
        this.resourceSupport = resourceSupport;
        this.employeeRepository = employeeRepository;
    }

    @GetMapping(value = "/employees")
    ResponseEntity<Resources<Resource<EmployeeCard>>> findAll() {

        List<Resource<EmployeeCard>> employeeResources =
                StreamSupport.stream(employeeRepository.findAll().spliterator(), false)
                        .map(resourceSupport::wrapResourceCollectionItem)
                        .collect(Collectors.toList());

        return ResponseEntity.ok(
                resourceSupport.wrapResources(
                        employeeResources,
                        linkTo(methodOn(EmployeeResource.class).findAll())
                                .withSelfRel(),
                        linkTo(methodOn(EmployeeResource.class).findAllByOrderBySalaryAsc())
                                .withRel("employees-sorted-by-salary"),
                        linkTo(methodOn(EmployeeResource.class).findAllByAgeYoungerThan(null))
                                .withRel("employees-younger-than")
                )
        );
    }

    @GetMapping(value = "/employees/sorted/salary")
    ResponseEntity<Resources<Resource<EmployeeCard>>> findAllByOrderBySalaryAsc() {

        List<Resource<EmployeeCard>> employeeResources =
                employeeRepository.findAllByOrderBySalaryAsc().stream()
                        .map(resourceSupport::wrapResourceCollectionItem)
                        .collect(Collectors.toList());

        return ResponseEntity.ok(
                resourceSupport.wrapResources(
                        employeeResources,
                        linkTo(methodOn(EmployeeResource.class).findAllByOrderBySalaryAsc())
                                .withSelfRel(),
                        linkTo(methodOn(EmployeeResource.class).findAll())
                                .withRel("employees"),
                        linkTo(methodOn(EmployeeResource.class).findAllByAgeYoungerThan(null))
                                .withRel("employees-younger-than")
                )
        );
    }

    @GetMapping(value = "/employees/younger/than/{age}")
    ResponseEntity<Resources<Resource<EmployeeCard>>> findAllByAgeYoungerThan(@PathVariable Integer age) {

        List<Resource<EmployeeCard>> employeeResources =
                employeeRepository.findAllByAgeLessThan(age).stream()
                        .map(resourceSupport::wrapResourceCollectionItem)
                        .collect(Collectors.toList());

        return ResponseEntity.ok(
                resourceSupport.wrapResources(
                        employeeResources,
                        linkTo(methodOn(EmployeeResource.class).findAllByAgeYoungerThan(age))
                                .withSelfRel(),
                        linkTo(methodOn(EmployeeResource.class).findAll())
                                .withRel("employees"),
                        linkTo(methodOn(EmployeeResource.class).findAllByOrderBySalaryAsc())
                                .withRel("employees-sorted-by-salary")
                )
        );
    }

    @GetMapping(value = "/employees/{id}")
    ResponseEntity<Resource<EmployeeRecord>> findOne(@PathVariable long id) {

        Employee employee = employeeRepository.findOne(id);

        if(Objects.isNull(employee))
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(resourceSupport.wrapResource(employee));
    }

}
