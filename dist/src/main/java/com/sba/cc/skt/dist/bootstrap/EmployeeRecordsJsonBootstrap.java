package com.sba.cc.skt.dist.bootstrap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sba.cc.skt.modules.employees.implementation.entities.Employee;
import com.sba.cc.skt.modules.employees.implementation.repository.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class EmployeeRecordsJsonBootstrap implements CommandLineRunner {

    private final ObjectMapper objectMapper;
    private final EmployeeRepository employeeRepository;

    public EmployeeRecordsJsonBootstrap(ObjectMapper objectMapper, EmployeeRepository employeeRepository) {
        this.objectMapper = objectMapper;
        this.employeeRepository = employeeRepository;
    }

    @Override
    public void run(String... args) {
        TypeReference<List<Employee>> typeReference = new TypeReference<List<Employee>>() {};
        InputStream inputStream = TypeReference.class.getResourceAsStream("/employees.json");
        try {
            List<Employee> users = objectMapper.readValue(inputStream,typeReference);
            employeeRepository.save(users);
        } catch (IOException e){
            throw new RuntimeException("Unable to save employees: " + e.getMessage());
        }
    }
}
