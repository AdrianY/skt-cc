SKT Coding Challenge: Backend Component of Solution by Adrian
=============================================================

This [my](https://ph.linkedin.com/in/adrianyago) solution for SKTs Coding Challenge. Upon deployment this will serve
a RESTful webservices that can be used to retrieve employee records. These webservices have
[maturity level 3](https://martinfowler.com/articles/richardsonMaturityModel.html). The format of 
the response is [HAL (Hypertext Application Language)](http://stateless.co/hal_specification.html).

The premise of this webservice is that its responsibility is to serve employee records stored in a JSON file. 
I tried to stay close with the requirement thus it only has four endpoints:

* /employees
* /employees/younger/than/{age}
* /employees/sorted/salary
* /employees/{id}

***Note:** The supported content type is <code>application/hal+json</code> only*

In order to deliver reliable results given the above specification, the following
technology stack is used:

* Spring Core - for beans lifecycle and IOC
* Spring Data Rest - to create HATEOAS RESTful webservices in HAL format
* Jackson - for serialization and JSON file parsing
* Spring boot - to manage Spring boot configuration and dependencies consistency
* Maven - software dependency, build, and modularisation

## Deployment prerequisite

1. You should have Maven 3 installed in the environment where you are going to try this. To check execute 
<code>mvn -v</code>.

2. For less hassle, free up your port 8080.

## Deploying the app

1. Build the application by executing <code>mvn clean install</code> in the root folder. This will create the target 
folders which contains the build files per each module.

2. Go to dist > target folder and run the application by using command <code>java -jar skt-cc-adrian-be.jar 
com.sba.cc.skt.dist.Runner</code>. You should be able to see the startup logs.

***Note:** if you want to deploy this separately just extract the lib and skt-cc-adrian-be.jar in dist > target folder then
execute the command above.*

## Testing the webservices

Upon deployment this application also serves a HAL Browser. To explore the webservices using this browser just go
to [*http://localhost:8080/browser/index.html*](http://localhost:8080/browser/index.html)